from . import views as v
from django.contrib.auth.views import TemplateView
from django.urls import include, path

app_name = "encrypter"

urlpatterns = [
    path("", v.PassView, name="home")
]
