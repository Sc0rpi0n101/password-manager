from . models import Passwords
from django import forms
from django.forms import ModelForm


class PassChoice(ModelForm):

    class Meta:
        model = Passwords
        fields = '__all__'
        widgets = {
            'password1': forms.PasswordInput(),
            'password2': forms.PasswordInput(),
            'password3': forms.PasswordInput(),
            'password4': forms.PasswordInput(),
            'password5': forms.PasswordInput(),
            'password6': forms.PasswordInput(),
            'password7': forms.PasswordInput(),
            'password8': forms.PasswordInput(),
            'password9': forms.PasswordInput(),
            'password10': forms.PasswordInput(),
        }
