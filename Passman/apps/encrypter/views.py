from django.shortcuts import render
from django.shortcuts import redirect, render, reverse
from django.utils import timezone
from .forms import PassChoice
# Create your views here.


def PassView(request):
    t = timezone.now()
    if request.method == "POST":
        form = PassChoice(request.POST)
        if form.is_valid():
            return redirect('/')

    else:
        formp = PassChoice()
        return render(request, "encrypter/index.html", {"form": formp})
