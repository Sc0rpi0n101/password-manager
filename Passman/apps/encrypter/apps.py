from django.apps import AppConfig


class EncrypterConfig(AppConfig):
    name = 'encrypter'
